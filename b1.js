


// BÀI 1: TÍNH NGÀY THÁNG NĂM
function ngayTruoc() {
    var a = document.getElementById("txt-ngay").value * 1;
    var b = document.getElementById("txt-thang").value * 1;
    var c = document.getElementById("txt-nam").value * 1;
    var d = document.getElementById("txtDate");
    var e = document.getElementById("txt-placeholder");
    var maxNgay = 31;
    var ngayTruoc = 0;
    var thangTruoc = 0;
    var namTruoc = 0;

    if (a > maxNgay || b > 12) {
        d.innerHTML = `Dữ liệu không hợp lệ`, e.innerHTML = ` `;
    } else if ((((c % 4 == 0 && (c % 100) != 0)) || ((c % 400) == 0)) && b == 2) {
        maxNgay = 29;
    } else if (b == 2) {
        maxNgay = 28;
    } else if (b == 4 || b == 6 || b == 9 || b == 11) {
        maxNgay = 30;
    } else {
        maxNgay = 31;
    }

    if (a != 1) {
        ngayTruoc = a - 1;
        thangTruoc = b;
        namTruoc = c;
    } else if (a == 1 && b != 1) {
        (b == 4 || b == 6 || b == 9 || b == 11) ? ngayTruoc = 30 : ngayTruoc = 31;
        thangTruoc = b - 1;
        namTruoc = c;
    } else if (a == 1 && b == 3) {
        ngayTruoc = 29;
        thangTruoc = 2;
        namTruoc = c;
    } else if (a == 1 && b == 3 && ((((c % 4 == 0 && (c % 100) != 0)) || ((c % 400) == 0)))) {
        ngayTruoc = 29;
        thangTruoc = 2;
        namTruoc = c;
    } else if (a == 1 && b == 1) {
        ngayTruoc = 31;
        thangTruoc = 12;
        namTruoc = c - 1;
    } else

        e.innerHTML = "Ngày, tháng, năm bạn cần: "
    d.innerHTML = `${ngayTruoc}/${thangTruoc}/${namTruoc}`
}

function ngaySau() {
    var a = document.getElementById("txt-ngay").value * 1;
    var b = document.getElementById("txt-thang").value * 1;
    var c = document.getElementById("txt-nam").value * 1;
    var d = document.getElementById("txtDate");
    var e = document.getElementById("txt-placeholder");
    var maxNgay = 31;
    var ngayTiep;
    var thangTiep;
    var namTiep;

    if (a <= maxNgay && b <= 12) {
        (b == 4 || b == 6 || b == 9 || b == 11) ? maxNgay = 30 : maxNgay = 31;

    } else if (b == 2) {
        maxNgay = 28;
    } else if ((((c % 4 == 0 && (c % 100) != 0)) || ((c % 400) == 0)) && b == 2) {
        maxNgay = 29;
    } else {
        d.innerHTML = `Dữ liệu không hợp lệ`, e.innerHTML = ` `;
    }

    switch (maxNgay) {
        case 28:
            (a == maxNgay) ? (ngayTiep = 1, thangTiep = 3, namTiep = c) : (ngayTiep = a + 1, thangTiep = b, namTiep = c);
            break;
        case 29:
            (a == maxNgay) ? (ngayTiep = 1, thangTiep = 3, namTiep = c) : (ngayTiep = a + 1, thangTiep = b, namTiep = c);
            break;
        case 30:
            (a == maxNgay) ? (ngayTiep = 1, thangTiep = b + 1, namTiep = c) : (ngayTiep = a + 1, thangTiep = b, namTiep = c);
            break;
        case 31:
            (a == maxNgay) ? (ngayTiep = 1, thangTiep = b + 1, namTiep = c) : (ngayTiep = a + 1, thangTiep = b, namTiep = c);
            (a == maxNgay && b == 12) ? (ngayTiep = 1, thangTiep = 1, namTiep = c + 1) : (ngayTiep = a + 1, thangTiep = b, namTiep = c);
            break;

        default:
            break;
    }
    e.innerHTML = "Ngày, tháng, năm bạn cần: "
    d.innerHTML = `${ngayTiep}/${thangTiep}/${namTiep}`
}
// () ? maxNgay = 29 : maxNgay = 28;
// () ? maxNgay = 30 : maxNgay = 31;

// (a > maxNgay || b > 12) ? (d.innerHTML = `Dữ liệu không hợp lệ`, e.innerHTML = ` `) : (e.innerHTML = "Ngày, tháng, năm bạn cần: ");
// if (a == maxNgay) {
//     if (b == 12) {
//         ngayTiep = 1, thangTiep = 1, namTiep = c + 1;
//     } else {
//         ngayTiep = a + 1, thangTiep = b, namTiep = c
//     }
// } else if (a > maxNgay)
//     (a == maxNgay) ? (ngayTiep = 1, thangTiep = b + 1, namTiep = c) : (ngayTiep = a + 1, thangTiep = b, namTiep = c);
// (a == maxNgay && b == 12) ? (ngayTiep = 1, thangTiep = 1, namTiep = c + 1) : (ngayTiep = a + 1, thangTiep = b, namTiep = c);




// BÀI 2: TÌM NGÀY CỦA THÁNG
function tinhNgay() {
    var b = document.getElementById("txt-tinh-thang").value * 1;
    var c = document.getElementById("txt-tinh-nam").value * 1;
    var d = document.getElementById("txt-tinh-Date");
    var e = document.getElementById("txt-tinh-placeholder");
    var maxNgay;

    if (b > 12) {
        d.innerHTML = `Dữ liệu không hợp lệ`, e.innerHTML = ` `;
    } else {
        switch (b) {
            case 1: case 3: case 5: case 7: case 8: case 10: case 12: maxNgay = 31;
                break;
            case 4: case 6: case 9: case 11: maxNgay = 30;
                break;
            case 2: (((c % 4 == 0 && (c % 100) != 0)) || ((c % 400) == 0)) ? maxNgay = 29 : maxNgay = 28;
                break;
            default:
        }
    }


    // (b == 4 || b == 6 || b == 9 || b == 11) ? maxNgay = 30 : maxNgay = 31;
    // ((((c % 4 == 0 && (c % 100) != 0)) || ((c % 400) == 0)) && b == 2) ? maxNgay = 29 : maxNgay = 28;


    d.innerHTML = `${maxNgay} ngày`;


}

// BÀI 3: 
function docSo() {
    var so = parseInt(document.getElementById("txt-so").value);
    var d = document.getElementById("txt-doc-so");
    var e = document.getElementById("txt-placeholder");
    if (so < 100 || so > 999) {
        d.innerHTML = `Vui lòng nhập số có 3 chữ số`;
    } else {
        var tram = (so - (so % 100)) / 100;
        var chuc = ((so % 100) - ((so % 100) % 10)) / 10;
        var dvi = so % 10;

        var docTram;
        var docChuc;
        var docDVi;
        switch (tram) {
            case 1:
                docTram = "Một trăm"
                break;
            case 2:
                docTram = "Hai trăm"
                break;
            case 3:
                docTram = "Ba trăm"
                break;
            case 4:
                docTram = "Bốn trăm"
                break;
            case 5:
                docTram = "Năm trăm"
                break;
            case 6:
                docTram = "Sáu trăm"
                break;
            case 7:
                docTram = "Bảy trăm"
                break;
            case 8:
                docTram = "Tám trăm"
                break;
            case 9:
                docTram = "Chín trăm"
                break;

            default:
                break;
        }
        switch (chuc) {
            case 0:
                docChuc = "lẻ"
                break;
            case 1:
                docChuc = "mười"
                break;
            case 2:
                docChuc = "hai mươi"
                break;
            case 3:
                docChuc = "ba mươi"
                break;
            case 4:
                docChuc = "bốn mươi"
                break;
            case 5:
                docChuc = "năm mươi"
                break;
            case 6:
                docChuc = "sáu mươi"
                break;
            case 7:
                docChuc = "bảy mươi"
                break;
            case 8:
                docChuc = "tám mươi"
                break;
            case 9:
                docChuc = "chín mươi"
                break;
            default:
                break;
        }
        switch (dvi) {
            case 0:
                docDVi = ""
                break;
            case 1:
                docDVi = "một"
                break;
            case 2:
                docDVi = "hai"
                break;
            case 3:
                docDVi = "ba"
                break;
            case 4:
                docDVi = "bốn"
                break;
            case 5:
                docDVi = "năm"
                break;
            case 6:
                docDVi = "sáu"
                break;
            case 7:
                docDVi = "bảy"
                break;
            case 8:
                docDVi = "tám"
                break;
            case 9:
                docDVi = "chín"
                break;
            default:
                break;
        }
        if (so % 100 == 0) {
            e.innerHTML = ` <p>Đọc bằng chữ: </p>`;
            d.innerHTML = docTram;
        } else if (so % 10 == 0) {
            e.innerHTML = `Đọc bằng chữ:  `;
            d.innerHTML = `${docTram} ${docChuc}`;
        } else {
            e.innerHTML = `Đọc bằng chữ:  `;
            d.innerHTML = `${docTram} ${docChuc} ${docDVi}`;
        }

    }
}
// BÀI 4:
function kCach(x1, y1, x2, y2) {
    var a = 0;
    a = Math.sqrt((x2 - x1) * (x2 - x1) + (y2 - y1) * (y2 - y1));
    return a;
}

// function Max3(a,b,c) {
//     var max;
//     if(a>b&&a>c){
//         max=a;
//     } else if (b>a&&b>c){
//         max=b;
//     } else{
//         max=c;
//     }
//     // (a>b&&a>c)?max=a:((b>a&&b>c)?max=b:max=c);
//     return max;

// }
function timSV() {
    var xt = document.getElementById("txt-x-truong").value * 1;
    var yt = document.getElementById("txt-y-truong").value * 1;
    var sv1 = document.getElementById("txt-sv1").value;
    var x1 = document.getElementById("txt-x-sv1").value * 1;
    var y1 = document.getElementById("txt-y-sv1").value * 1;
    var t1 = kCach(x1, y1, xt, yt);
    var sv2 = document.getElementById("txt-sv2").value;
    var x2 = document.getElementById("txt-x-sv2").value * 1;
    var y2 = document.getElementById("txt-y-sv2").value * 1;
    var t2 = kCach(x2, y2, xt, yt);
    var sv3 = document.getElementById("txt-sv3").value;
    var x3 = document.getElementById("txt-x-sv3").value * 1;
    var y3 = document.getElementById("txt-y-sv3").value * 1;
    var t3 = kCach(x3, y3, xt, yt);

    var tenSV;
    var d = document.getElementById("txt-result");
    if (t1 > t2 && t1 > t3) {
        tenSV = sv1
    } else if (t2 > t1 && t2 > t3) {
        tenSV = sv2;
    } else { tenSV = sv3 }

    d.innerHTML = tenSV;

}

